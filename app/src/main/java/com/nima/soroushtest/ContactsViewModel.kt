package com.nima.soroushtest

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList

class ContactsViewModel constructor(application: Application)
    : AndroidViewModel(application) {

    private var contactssLiveData: LiveData<PagedList<ContactModel>>

    init {
        val factory: DataSource.Factory<Int, ContactModel>? =
            ContactsDB.getInstance(getApplication())?.contactsDataDAO()?.getAllPaged()

        val pagedListBuilder: LivePagedListBuilder<Int, ContactModel> = LivePagedListBuilder<Int, ContactModel>(
            factory!!,
            50)
        contactssLiveData = pagedListBuilder.build()
    }

    fun getContactsLiveData() = contactssLiveData
}