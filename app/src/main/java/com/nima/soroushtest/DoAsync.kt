package com.nima.soroushtest

import android.os.AsyncTask

class DoAsync(val handler: () -> Unit, val handler2: () -> Unit) : AsyncTask<Void, Void, Void>() {
    override fun doInBackground(vararg params: Void?): Void? {
        handler()
        return null
    }

    override fun onPostExecute(result: Void?) {
        handler2()
    }
}