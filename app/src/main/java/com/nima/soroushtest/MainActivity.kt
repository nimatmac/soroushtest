package com.nima.soroushtest

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View


class MainActivity : AppCompatActivity() {
    private val PERMISSION_REQUEST_CODE = 1

    lateinit var errorText: AppCompatTextView
    lateinit var recycler: RecyclerView

    private lateinit var viewModel: ContactsViewModel

    private lateinit var adapter: ContactsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        errorText = findViewById(R.id.permissionError)
        recycler = findViewById(R.id.recycler)

        errorText.setOnClickListener {
            errorText.visibility = View.GONE
            checkContactsPermission()
        }

        viewModel = ViewModelProviders.of(this).get(ContactsViewModel::class.java)


        checkContactsPermission()
        showDataOnList()
    }

    private fun showDataOnList() {
        recycler.layoutManager = LinearLayoutManager(this)
        adapter = ContactsAdapter()
        recycler.adapter = adapter
        getDataFromDB()
    }

    private fun getDataFromDB() {
        viewModel.getContactsLiveData().observe(this, Observer { names ->
            if (names != null) adapter.submitList(names)
        })
    }

    private fun checkContactsPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val startIntent = Intent(applicationContext, DataService::class.java)
            startIntent.action = "Start"
            startService(startIntent)
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST_CODE)
        }
    }

    private fun showPermissionError() {
        errorText.visibility = View.VISIBLE
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            checkContactsPermission()
        } else {
            showPermissionError()
        }
    }
}
