package com.nima.soroushtest

import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class NumbersAdapter(private var item: ContactModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ViewHolder : RecyclerView.ViewHolder {
        var phone: AppCompatTextView
        constructor(itemView: View) : super(itemView) {
            phone = itemView.findViewById(R.id.phone)
        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.numbers_adapter_item, p0, false))
    }

    override fun getItemCount(): Int {
        return item.phone.size
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = p0 as ViewHolder
        viewHolder.phone.text = item.phone[position]
    }

}