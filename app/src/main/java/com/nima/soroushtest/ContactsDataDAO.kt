package com.nima.soroushtest

import android.arch.paging.DataSource
import android.arch.persistence.room.*

@Dao
interface ContactsDataDAO {

    @Query("SELECT * FROM contacts")
    fun getAll(): List<ContactModel>

   @Query("SELECT * FROM contacts")
    fun getAllPaged(): DataSource.Factory<Int, ContactModel>

    @Query("SELECT * FROM contacts WHERE id = :id")
    fun get(id : String): ContactModel

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(contactModel: ContactModel) : Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(contact: ContactModel)

    @Query("DELETE from contacts")
    fun deleteAll()

    @Query("DELETE from contacts WHERE id = :id")
    fun delete(id : String)
}