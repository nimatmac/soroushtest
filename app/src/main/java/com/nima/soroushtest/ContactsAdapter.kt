package com.nima.soroushtest

import android.arch.paging.PagedListAdapter
import android.content.Intent
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ContactsAdapter : PagedListAdapter<ContactModel, ContactsAdapter.ViewHolder>(ContactDiffCallback()) {

    class ViewHolder : RecyclerView.ViewHolder {
        var nameTv: AppCompatTextView
        var phoneTv: AppCompatTextView

        constructor(itemView: View) : super(itemView) {
            nameTv = itemView.findViewById(R.id.nameTv)
            phoneTv = itemView.findViewById(R.id.phoneTv)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.contact_adapter_item, p0, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.nameTv.text = "$position ${getItem(position)?.name}"
        if (!getItem(position)?.phone?.isEmpty()!!)
            viewHolder.phoneTv.text = getItem(position)?.phone!![0]
        else
            viewHolder.phoneTv.text = ""

        viewHolder.itemView.setOnClickListener {
            val intent = Intent(viewHolder.itemView.context, DetailedActivity::class.java)
            intent.putExtra("data", getItem(position))
            viewHolder.itemView.context.startActivity(intent)
        }
    }

}