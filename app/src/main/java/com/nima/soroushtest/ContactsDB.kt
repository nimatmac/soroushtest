package com.nima.soroushtest

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

@Database(entities = arrayOf(ContactModel::class), version = 1)
@TypeConverters(DBConverter::class)
abstract class ContactsDB : RoomDatabase() {

    abstract fun contactsDataDAO(): ContactsDataDAO

    companion object {
        private var INSTANCE: ContactsDB? = null

        fun getInstance(context: Context): ContactsDB? {
            if (INSTANCE == null) {
                synchronized(ContactsDB::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        ContactsDB::class.java, "contacts.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}