package com.nima.soroushtest

import android.content.Context
import android.content.Intent
import android.database.ContentObserver
import android.net.Uri
import android.util.Log

class MyContentObserver(private var context: Context) : ContentObserver(null) {

    override fun onChange(selfChange: Boolean, uri: Uri?) {
        super.onChange(selfChange, uri)
        Log.e("Tag", "is Self Change : $selfChange , url = $uri")
        val startIntent = Intent(context.applicationContext, DataService::class.java)
        startIntent.action = "Reload"
        context.startService(startIntent)

    }

    override fun deliverSelfNotifications(): Boolean {
        return true
    }
}