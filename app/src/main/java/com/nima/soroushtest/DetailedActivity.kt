package com.nima.soroushtest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

class DetailedActivity : AppCompatActivity() {

    lateinit var title: AppCompatTextView
    lateinit var close: AppCompatTextView
    lateinit var recycler: RecyclerView

    private lateinit var adapter: NumbersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)

        title = findViewById(R.id.name)
        close = findViewById(R.id.close)
        recycler = findViewById(R.id.recycler)

        close.setOnClickListener { finish() }

        val data = intent.getSerializableExtra("data") as ContactModel

        title.text = data.name

        recycler.layoutManager = LinearLayoutManager(this)
        adapter = NumbersAdapter(data)
        recycler.adapter = adapter
    }
}
