package com.nima.soroushtest

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DBConverter {

    @TypeConverter
    fun stringTophone(json: String): List<String>? {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {
        }.type
        return gson.fromJson<List<String>>(json, type)
    }

    @TypeConverter
    fun phoneToString(list: List<String>): String {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {

        }.type
        return gson.toJson(list, type)
    }
}
