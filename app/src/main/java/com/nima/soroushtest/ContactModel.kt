package com.nima.soroushtest

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "contacts")
data class ContactModel(@PrimaryKey var id: String, @ColumnInfo(name = "name") var name: String, @ColumnInfo(name = "phone") var phone: List<String>) : Serializable