package com.nima.soroushtest

import android.Manifest
import android.app.Service
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.os.IBinder
import android.provider.ContactsContract
import android.support.v4.content.ContextCompat

class DataService : Service() {

    lateinit var cr: ContentResolver

    private var mDb: ContactsDB? = null

    private lateinit var contentObserver: MyContentObserver

    companion object {
        @JvmStatic
        var isServiceRunning = false
        @JvmStatic
        var isContactsReading = false
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        init()
        isServiceRunning = true
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.action == "Start") {
            init()
        } else if (intent != null && intent.action == "Reload") {
            loadContacts()
        } else stopMyService()
        return START_STICKY
    }

    private fun init() {
        if (isServiceRunning)
            return

        mDb = ContactsDB.getInstance(this)

        contentObserver = MyContentObserver(this)

        cr = contentResolver
        cr.registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, contentObserver)

        loadContacts()

    }

    private fun loadContacts() {
        if (isContactsReading)
            return

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            stopMyService()
            return
        }
        isContactsReading = true
        val resolved : ArrayList<ContactModel> = ArrayList()
        val cur = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        val count = when (cur) {
            null -> 0
            else -> cur.count
        }
        if (count > 0) {
            while (cur != null && cur.moveToNext()) {
                val id = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID)
                )
                val name = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )

                if (cur.getInt(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER
                        )
                    ) > 0
                ) {
                    val pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id), null
                    )
                    val phones: ArrayList<String> = ArrayList()
                    while (pCur.moveToNext()) {
                        val phoneNo = pCur?.getString(
                            pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                            )
                        )
                        phones.add(phoneNo!!)
                    }
                    var contactModel = ContactModel(id, name, phones)
                    resolved.add(contactModel)
                    DatabaseUtils.insertIntoDB(mDb, contactModel)
                    pCur?.close()
                }
            }
        }
        cur?.close()
        DoAsync({
            val data = DatabaseUtils.getDataFromDB(mDb)
            val a = HashSet<ContactModel>(data)
            a.removeAll(resolved)
            for (c in a) {
                DatabaseUtils.removeItemFromDB(mDb, c)
            }
        },
            {}).execute()

        isContactsReading = false
    }

    override fun onDestroy() {
        isServiceRunning = false
        cr.unregisterContentObserver(contentObserver)
        super.onDestroy()
    }

    private fun stopMyService() {
        stopForeground(true)
        stopSelf()
        isServiceRunning = false
    }
}
