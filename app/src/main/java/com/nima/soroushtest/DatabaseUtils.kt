package com.nima.soroushtest

class DatabaseUtils {
    companion object {
        fun insertIntoDB(mDb: ContactsDB?, contactModel: ContactModel) {
            DoAsync({
                val id = mDb?.contactsDataDAO()?.insert(contactModel)
                if (id == (-1).toLong()) {
                    val toBeEdited = mDb?.contactsDataDAO()?.get(contactModel.id)
                    toBeEdited?.name = contactModel.name
                    toBeEdited?.phone = contactModel.phone
                    mDb?.contactsDataDAO()?.update(toBeEdited!!)
                }
            }, {}).execute()
        }

        fun getDataFromDB(mDb: ContactsDB?) : List<ContactModel>? {
            return mDb?.contactsDataDAO()?.getAll()
        }

        fun removeItemFromDB(mDb: ContactsDB?, contactModel: ContactModel) {
            mDb?.contactsDataDAO()?.delete(contactModel.id)
        }
    }
}